
/*
	GitBash:
	npm init -y
	npm install express
	npm install mongoose
	npm install cors

*/

const express = require("express");
const mongoose = require("mongoose");

// to create an express server/ application
const app = express();

// Middlewares - allows to bridge our backend application (server) to our front end

// to allow cross origin resource sharing
app.use(cors()); 
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}))

// Connect to our MongoDB Database

mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.m7vkucn.mongodb.net/courseBooking?retryWrites=true&w=majority", {(
	useNewUrlParser: true,
	useUnifiedTopology: true
)}); 

// Prompts a message once connected 
mongoose.connection.once('open', () => console.log('Now connected to Arellano-Mongo DB Atlas.'))

app.listen(process.env.PORT || 4000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000 }`)
});

// 3000, 4000, 5000, 8000 - Port numbers for Web applications



